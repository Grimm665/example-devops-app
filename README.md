# Monadical Dev-Ops Take-Home Project

This is the example application to be used by interviewees applying for a Dev-Ops engineer position at Monadical.

As outlined here:
https://docs.zalad.io/s/monadical-study-guide#Dev-Ops-Project

## Quickstart

1. Purchase the cheapest $3.50/mo VPS with a public IP on Vultr.com (or DigitalOcean)
2. Buy a cheap domain via Google Domains, Namecheap, or another registrar and point it to your VPS
3. Fork this repo, and clone your version into `/opt/banana` on the VPS
  `git clone https://github.com/<yourusername>/example-devops-app /opt/banana`
4. Begin setting up the server (paths below are relative to `/opt/banana`)
  - Put all your config in `etc/` (e.g config for `nginx`, `supervisor`, etc)
  - Put all mutable data in `data/` (e.g. `nginx` certs, `postgres` database, logs)
  - Put all management scripts in `bin/` (e.g. `start`, `stop`, `deploy`, `backup`)
  - Put all application code in `code/` (e.g. django app code, python virtualenv)

Empty stub files are provided in the recommended directory structure to help speed up the process.
Make sure to edit them with your code and commit all config, scripts, and code created during the setup process to your fork of this repo.

Follow the structure oulined in this post:
https://docs.zalad.io/s/an-intro-to-the-opt-directory

## Deployment steps:

Deployment will be described from "inside-out", starting with the python app and working outwards to the server platform:

### Requirements.txt and settings.py

We add `Django` as a core dependency, and we also add `django-environ` and `psycopg2-binary` to manage our database connection. Specifically, `django-environ` allows us to move the database connection definition out of our Python app to make the app more db-agnostic.

In the settings.py, we import the `environ` library and modify the default database connection to disable sqlite, and include the connection to postgres we define in our `docker-compose.yml` file.

### Dockerfile

We deploy the python app using the official `python:3.9.6-slim` container. We can use the slim image since this app is small and uncomplicated.

The Dockerfile is simple, just setting the working directory and deploying our python dependencies.

### docker-compose.yml

Most of the work is being done in the `docker-compose.yml` file. We first declare the database container using the `postgres:13-alpine` container. Alpine is light, fast, and secure, a perfect platform for postgres to run on in this scenario. We expose the default port `5432` and add a health check to signal the web app when it is ready to accept connections. For our username, password, and database name, we use a `.env` file to define these variables.

Next we declare the `banana` app itself using the Dockerfile located in the `code` directory. We define our database connection with an environment variable that will be read by the `environ` library. We expose port 8000, and wait for the database dependency to become healthy using the health check defined above. Finally, we run any migrations that might be needed, and start the app server.

### .env_sample

This is a template for the `.env` file, it should be copied and modified as appropriate for the deployment.

    cp .env_sample .env
    vim .env
